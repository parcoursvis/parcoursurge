# ParcoursURGE

![ParcoursURGE](parcoursvis.png)

ParcoursURGE is an adaptation of [[ParcoursVis]](https://gitlab.inria.fr/aviz/parcoursvis) for the URGE project. It supports the visual analysis of patients' care pathways at emergency facilities from AP-HP hospitals.

## Installation

Retrieve the distribution on gitlab:

`git clone https://gitlab.inria.fr/misereno/parcoursurge.git`

Then, connect to the `parcoursvis` directory and update the submodules:

```
git submodule init
git submodule update
```


### With miniconda/anaconda (recommended):

1. Install the latest version of miniconda

You can access it at (https://docs.conda.io/en/latest/miniconda.html)

2. Create a conda environment with the following command:

With a linux system:
```
cd back
conda env create -f environment.yml
```

with a Mac OSX system:
```
cd back
conda env create -f environment-osx.yml
```


3. Activate this environment:

```
conda activate parcoursurge
```

4. Execute the following commands (still connected to the `back` directory):

```
make
```

This compiles the C++ command-line programs used to prepare the dataset, and installs locally the dynamic library for the C++ code used by python. It also compiles the Javascript/Vuejs program used for the front-end.

At this point, the whole environment is compiled and ready to run.  One example dataset is also processed and ready to use in the `test10000` directory.

## Compiling test units

1. You will need to install, through anaconda,:
- gtests
- cmake
- jsoncpp
- pkgconfig

```
conda install -c anaconda -n parcoursurge cmake pkgconfig
conda install -c conda-forge -n parcoursurge gtest jsoncpp
```

2. Go to "back/parser" and create a "build" directory.

3. Compile the unit test: 

```
cd build/
cmake ../ -G "Unix Makefiles" #init cmake and create a Makefile
ln -s ../tests . #Create a symbolic link to dumb data
make #compile
```

4. Run the unit test:

```
./pparserUnitTests
```

## Parsing and compiling the dataset

A dataset is a csv file with the following first lines:
```
NUM_ENQ,age,category,UTC,subservice,Other diseases
1,37,box1,1024,secretary,Diabete
1,37,box1,1100,doctor,Diabete
...
2,73,box2,5874,hall1,Hypertension
2,73,box2,5980,nurse,Hypertension
...
```

This file can be interpreted as follows: The first line specifies that the file is made of five columns. Each line describes an event status change that happened to one patient at one time.
1. `NUM_ENQ` is the pseudo-anonymous number assigned to one patient
1. `age` is the age of the patient at the date of the treatment
1. `category` is the category of the treatment, situation, or drug consumption of the patient.
1. `UTC` is the date (in UTC UNIX time) of the treatment, drug, or change of situation of the patient.
1. `Other diseases` if comorbidity factors are known, such as diabetes, hypertension, overweight, etc. they can be added to that last columns. They should be listed as *space separated* names.

### Processing the CSV File

This csv should be processed to be used by the parcoursurge system. Assuming the csv file is called `filename.csv` and is in the `data` directory, execute the following command:

```
./parser/preparser data/filename.csv
```

If the file is compressed because it is very big, assuming it is named `filename.csv.bz2` in the `data` directory, then execute the following commands instead:
```
bzcat data/filename.csv.bz2 | ./parser/preparser -
```
Note the minus-sign (-) at the end of the `preparser` command to mean "read data from standard input".

The results are stored in a subdirectory called `tmp` by default. This directory will be used by the python server by default.

Optionally, a second argument can be given to the `preparser` command to store the result in a directory with the specified name instead of `tmp`.

Note that those CSV files should follow the format described earlier. If your initial data are from the simulation part of the overall project, use the python script ```back/generate_data/convert_from_simulation.py```

At this point, you are ready to run the Python and JavaScript servers and play with the data on your browser.

## Checking the results

You can check that the csv file has been processed correctly by running the `deparser` program:

```
./parser/deparser tmp > out.csv
```

The resulting csv file should be identical to the original file. You can test it with the unix `cmp` program:

```
bzcat data/filename.csv.bz2 | cmp - out.csv
```

## Running the environment locally

Rename the file ./front/.env.example to ./front/.env.local and set the IP of your machine, such as: `VUE_APP_IP_VM=127.0.0.1`. 
Then, depending on whether you are relying on a SSL or non-encrypted connection, you need to change the adequate environment variable (or both if you want to implement both features):

`VUE_APP_PORT_VM_WSS=443`
`VUE_APP_PORT_VM_WS=5678`

Finally, if you have multiple webpages within the same domain, you can change the value of "VUE\_APP\_SERVER\_PREFIX" to add a prefix to parcoursURGE


There are two services that should run: the python back-end and the javascript back-end.

To run the python back-end in background, execute the following command from the main directory (on Linux or MacOS):
```
cd back
python parser/index.py test10000 &
```

To tun the javascript back-end in foreground, execute the following command from the main directory:
```
unset HOST
cd front
npm run serve
```

Then, you can connect to the interface from a web browser by opening the url `localhost:8080/`.

If you can see the output of the Python server, it should tell you that the connection has been established.

If you have created a data directory with a specified name, you can use it by running the `index.py` program with the name as argument.

## Testing

To test if the programs works, you can run the following command:

```
make run
```
It will generate a test in the `tmp` directory and run both the python and vue servers. You can test it from a web browser by opening the url `localhost:8080/`.

## Deploy on a virtual machine

We rely on docker to generate and run all the components (backend, frontend, and nginx) that exchange information through the docker virtual network. Nevertheless, the file "front/.env.local" should be configured correctly, with, e.g., 

```VUE_APP_IP_VM=<serverURL>```

It is also possible to run all the different subcomponents (the backend and frontend) on a distant virtual machine, and to rely on nginx (see the folder nginx) to create a proxy server that either delivers the websocket (through the URL server/ws) and the frontend application (through the URL server/). This is actually what our docker-compose script is doing after having (1) compiled all the components, and (2) having preparsed the dataset if this was not yet done. You ***SHOULD NOT*** appends in the file "front/.env.local" the "/ws" suffix: This is done automatically by our application (see src/plugins/vuex.js for more information).

1. Install docker and docker-compose on the virtual machine (see the documentation of your linux distribution)

2. Enable docker daemon (as root) if not done: 
```
# systemctl enable docker
# systemctl start docker
```

3. Create the docker group and add "user" to this group if not done:
```
# groupadd docker
# usermod -aG docker "user" #Change "user" by the user's name
```

4. Log in / Log out for changes to take effects, or run (if possible) as user "user":
`$ newgrp docker`

5. Create two directories "./back/tmp" (for temporary files to gain time), and "./back/data" where your data will be:
```
$ mkdir ./back/tmp
$ mkdir ./back/data
```

The docker-compose will automatically link these two directories with the corresponding docker (see docker-compose.yml for more details).

6. To use your datasets, you can (by order of priority):
- have them already parsed (using preparser) in the folder ./back/tmp.
- have a file ./back/data/file.csv representing your data. The server will then parse those data and store them in ./back/tmp for optimization between multiple running
- have a file ./back/data/file.csv.bz2 representing your data. The server will then parse those data and store them in ./back/tmp for optimization between multiple running

Note that file.csv and file.csv.bz2 should follow the CSV file format ParcoursURGE relies on. Use the python script in ```parser/generate_data/convert_from_simulation.py``` to convert data from the simulation team to our own CSV file format.

7. Rename the file ./front/.env.example to ./front/.env.local and set the IP of your virtual machine. 
The server will automatically use the port 5678.

8. Build and run the docker-compose file https://docs.docker.com/compose/production/: 
```
docker-compose build
docker-compose up
```

9. Open your browser and use the IP address of the machine running the virtual machine of the docker-compose.
You can:
- Connect directely to the http-server of the "front" application using the port 8080. 
- Use the server "nginx" that the docker compose is running, and connect yourself directly to http://IPaddress/ (e.g., http://localhost/)

10. If you want to assign different port to your docker, modify the port redirection in docker-compose.yml. Remember to modify also the ports in ./front/.env.local and in ./nginx/nginx.conf (if you are relying on nginx)

## Use our pre-generated docker images from the gitlab registry

1. Repeat step 1 to 6 as previously explained in "Deploy on a virtual machine".

2. Launch  the sh script ```launchGitlabDocker.sh```

3. Open your browser and open the webpage ```localhost:8080```
