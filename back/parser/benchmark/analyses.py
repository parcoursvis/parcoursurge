from cycler import cycler
import sys
import os
import json
import scipy
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.ticker as ticker

def printDocAndExit():
    """
    Print the documentation of how to use the script and exit the program.
    """
    print("Run ./analyses.py [inFile] [--output directoryPath]")
    print("                   [-h] [--help]")
    print("inFile: the json file to analyse that benchmark.py outputs. Default: ${pwd}/output.json")
    print("--output: output directory to save the different PDF data analyses results. Default: ${pwd}/output/")
    print("-h --help: Print this documentation and exit.")

    sys.exit(-1)

def initFigure():
    """
    Initialize matplotlib figures common for our data analyses
    Return a tuple of (matplotlib.Figure, matplotlib.Axes)
    """
    fig = plt.figure(figsize=(10,2))
    fig.set_tight_layout(True)
    ax = fig.add_subplot(aspect="auto")
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)

    return fig, ax

def printTime(data, outputDir):
    """
    Save the time performance in outputDir/time.pdf
    This computes the 95% CIs of the time it takes to aggregate chunkSize patients per iteration
    """
    #Gather the values we are interested in
    prog  = [x for x in data["Progressive"] if len(x['alignSequence']) == 0]
    columns = ['time', 'chunkSize', 'threshold']
    timeProg = pd.DataFrame(columns=columns)
    for x in prog:
        for it in x['iterations']:
            timeProg = pd.concat([timeProg, pd.DataFrame([[it['time'], x['chunk_size'], x['threshold']]], columns=columns)], ignore_index=True)


    print("####### TIME COMPUTATION ########")
    chunkSizes = timeProg.chunkSize.unique()
    fig, ax = initFigure()
    values = []
    confidenceIntervals = [[], []]
    for chunkSize in chunkSizes:
        #Bootstrap the time
        #We suppose that time is a skewed distribution, hence the log/exp data analysis
        subTimeData   = timeProg[timeProg['chunkSize'] == chunkSize]['time']
        timeBootstrap = scipy.stats.bootstrap((np.log(subTimeData),), np.mean, n_resamples=10000, confidence_level=0.95, method='bca', random_state=0)
        confidenceInterval = np.exp(timeBootstrap.confidence_interval)
        value = np.exp(np.mean(np.log(subTimeData)))

        #Create and configure the plot
        values.append(value)
        confidenceIntervals[0].append(value-confidenceInterval[0])
        confidenceIntervals[1].append(confidenceInterval[1]-value)

        print(f"chunkSize: {chunkSize}, value: {value}, confidence_intervals: {confidenceInterval}")

    ax.errorbar(x=values, y=range(len(chunkSizes)), xerr=confidenceIntervals, fmt='o')
    ax.set_yticks(range(len(chunkSizes)))
    ax.set_yticklabels(chunkSizes)

    outputFile = f"{outputDir}/time.pdf"
    print(f"Saving {outputFile}...\n")
    fig.savefig(outputFile, format="pdf", bbox_inches='tight')

def printProgressiveStability(data, outputDir):
    """
    Save the stability data analyses of both the Count and Hysteresis sorting method
    Output the file {outputDir}/stability.pdf and {outputDir}/PWstability (pairwise comparison between the Count and Hysteresis conditions).
    """
    # ------------------------------------------------------------------------------
    # ---------------Analyze the iteration where stability is reached---------------
    # ------------------------------------------------------------------------------
    prog  = [x for x in data["Progressive"] if len(x['alignSequence']) == 0]
    columns = ['stableAt', 'method', 'chunkSize', 'threshold', 'depth']
    stats = pd.DataFrame(columns=columns)
    for x in prog:
        for it in x['stats']:
            stats = pd.concat([stats, pd.DataFrame([[it['stableWithoutTopologyAtIter']/(len(x['iterations'])-1), \
                                                    it['sortingMethod'], x['chunk_size'], x['threshold'], it['depth']]], columns=columns)],\
                              ignore_index=True)

    #Compute pairwise comparisons
    byCountStats      = stats[stats['method'] == "Count"]
    byHysteresisStats = stats[stats['method'] == "Hysteresis"]
    pairwiseStats     = pd.merge(byCountStats, byHysteresisStats, on=["chunkSize", "threshold", "depth"], suffixes=["_x", "_y"])
    pairwiseStats["stableAt"] = pairwiseStats["stableAt_x"] - pairwiseStats["stableAt_y"]
    pairwiseStats["method"]   = "Count-Hysteresis"

    def drawFigure(fig, ax, data, legend, offset=0):
        """
        Helper function to analyze and draw the statistics of the stability of the current dataframe.
        The dataframe should associate a stableAt column with a depth column. This function will not
        perform any filtering on the original dataframe.
        """
        depths = pd.unique(data['depth'])
        values = []
        confidenceIntervals = [[], []]
        for depth in depths:
            depthSubStats = data[data['depth'] == depth]
            value     = np.mean(depthSubStats['stableAt'])
            if np.all(depthSubStats['stableAt'] == value):
                confidenceInterval = [value, value]
            else:
                bootstrap = scipy.stats.bootstrap((depthSubStats['stableAt'],), np.mean, n_resamples=10000, confidence_level=0.95, method='bca', random_state=0)
                confidenceInterval = bootstrap.confidence_interval
            values.append(value)
            confidenceIntervals[0].append(value-confidenceInterval[0])
            confidenceIntervals[1].append(confidenceInterval[1]-value)
            
            print(f"Method: {legend}, depth: {depth}, value: {value}, confidence_intervals: {list(confidenceInterval)}")

        print(confidenceIntervals)
        ax.errorbar(x=values, y=depths+offset, xerr=confidenceIntervals, fmt='o', label=legend)

        fig.set_size_inches(10, 2)
        ax.set_yticks(depths)
        ax.set_yticklabels(depths)

    #Draw figures for raw data distribution
    print("####### STABILITY COMPUTATION ########")
    fig, ax = initFigure()
    for method, data, offset in zip(['Count', 'Hysteresis'], [byCountStats, byHysteresisStats], [-0.1, +0.1]):
        drawFigure(fig, ax, data, method, offset)
    ax.legend()
    outputFile = f"{outputDir}/stability.pdf"
    print(f"Saving {outputFile}...")
    fig.savefig(outputFile, format="pdf", bbox_inches='tight')

    #Draw the pairwise comparison figure
    fig, ax = initFigure()
    drawFigure(fig, ax, pairwiseStats, "Count-Hysteresis")
    ax.axvline(x=0.0)
    ax.legend()
    outputFile = f"{outputDir}/PWstability.pdf"
    print(f"Saving {outputFile}...\n")
    fig.savefig(outputFile, format="pdf", bbox_inches='tight')

def printProgressiveNbPermutations(data, outputDir):
    """
    Save the data analyses about the number of permutations for both the Count and Hysteresis sorting method, and per number of threshold
    Output multiple files templated {outputDir}/nbPermutations_{method}_threshold_{threshold}.pdf and {outputDir}/PWnbPermutations_threshold_{threshold} (pairwise comparison between the Count and Hysteresis conditions).
    """
    print("####### PERMUTATION COMPUTATION ########")

    prog  = [x for x in data["Progressive"] if len(x['alignSequence']) == 0]
    columns = ['itID', 'nbPermutations', 'method', 'chunkSize', 'threshold', 'depth', 'nbIteration']
    stats = pd.DataFrame(columns=columns)
    for x in prog:
        for it in x['stats']:
            for itID, detail in enumerate(it['data']):
                stats = pd.concat([stats, pd.DataFrame([[itID, detail['permutations'], it['sortingMethod'], x['chunk_size'], x['threshold'], \
                                                        it['depth'], len(x['iterations'])]], columns=columns)],\
                                  ignore_index=True)
    byCountStats      = stats[stats['method'] == "Count"]
    byHysteresisStats = stats[stats['method'] == "Hysteresis"]
    uniqueChunkSize = pd.unique(stats['chunkSize'])

    def drawPermutationFigure(data, fig, ax):
        for _, subData in data.groupby(['chunkSize']):
            xCoordinates = range(subData['nbIteration'].iat[0]-1) #All the nbIteration should be equal

            for depth, depthSubData in subData.groupby(['depth']):
                yCoordinates = depthSubData.groupby(['itID'])['nbPermutations'].median()
                ax.plot(xCoordinates, yCoordinates, label=f"depth = {depth}", color=f'C{depth}')
                handles, labels = fig.gca().get_legend_handles_labels()
                by_label = dict(zip(labels, handles))
                ax.legend(by_label.values(), by_label.keys())
        ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
        ax.yaxis.set_major_locator(ticker.MultipleLocator(10))
        ax.set_xlabel("Iteration #")
        ax.set_ylabel("Number of Permutations")

    #Draw a line chart per depth
    for method, data in zip(['Count', 'Hysteresis'], [byCountStats, byHysteresisStats]):
        fig, ax = initFigure()
        
        for threshold,thresholdSubData in data.groupby("threshold"):
            drawPermutationFigure(thresholdSubData, fig, ax)
            outputFile = f"{outputDir}/nbPermutations_{method}_threshold_{threshold}.pdf"
            print(f"Saving {outputFile}...\n")
            fig.savefig(outputFile, format="pdf", bbox_inches='tight')

    #Now perform a pairwise comparison
    pairwiseStats = pd.merge(byCountStats, byHysteresisStats, on=["itID", "chunkSize", "threshold", "depth", "nbIteration"], suffixes=["_x", "_y"])
    pairwiseStats["nbPermutations"] = pairwiseStats["nbPermutations_x"] - pairwiseStats["nbPermutations_y"]
    pairwiseStats["method"]   = "Count-Hysteresis"

    fig, ax = initFigure()
    for threshold,thresholdSubData in pairwiseStats.groupby("threshold"):
        drawPermutationFigure(thresholdSubData, fig, ax)
        outputFile = f"{outputDir}/PWnbPermutations_threshold_{threshold}.pdf"
        print(f"Saving {outputFile}...\n")
        fig.savefig(outputFile, format="pdf", bbox_inches='tight')


# ------------------------------------------------------------------------------
# -----------------------------------The Main-----------------------------------
# ------------------------------------------------------------------------------
if __name__ == '__main__':
    argv      = sys.argv[1:]
    outputDir = os.getcwd() + "/output"
    inFile    = "output.json"

    #Read argv
    i = 0
    while i < len(argv):
        arg = argv[i]
        if i == 0 and not (arg.startswith("--") or arg.startswith("-")):
            inFile = arg
            i+=1
        else:
            while i < len(argv):
                arg = argv[i]

                if arg == "-h" or arg == "--help":
                    printDocAndExit()

                elif arg == "--output":
                    if i < len(argv)-1:
                        if argv[i+1][0] == '-':
                            print(f"Missing the directory path value of the '--output' parameter. {argv[i+1]} is not a correct value. Exiting.")
                            sys.exit(-1)
                        outputDir = argv[i+1]
                        i+=1
                    else:
                        print(f"Missing the directory path value of the '--output' parameter. Exiting.")
                        sys.exit(-1)
                else:
                    print(f"Unknown parameter {arg}")
                    printDocAndExit()
                i+=1

    #Check that the input file exists and read it
    assert os.path.isfile(inFile)
    data = None
    with open(inFile, 'r') as f:
        data = json.load(f)

    #Create the output directory, if necessary
    if not os.path.exists(outputDir):
        os.mkdir(outputDir)

    #Shallow tests to check that we have parsed the correct JSON
    #The test can be done more in-depth (check that NonProgressive and Progressive have the correct data entries). But this is overkilled
    #for our use-cases
    for x in "NonProgressive", "Progressive":
        assert x in data

    #Set the default color palette to Dark2 (color brewer)
    plt.rcParams['axes.prop_cycle'] = cycler(color=cm.Dark2.colors)

    #Perform the data analyses on the JSON file, and save the results as PDF files
    printTime(data, outputDir)
    printProgressiveStability(data, outputDir)
    printProgressiveNbPermutations(data, outputDir)
