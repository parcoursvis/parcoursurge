import sys
import os
import json
import logging
import copy
import time
import csv
from functools import *
from itertools import *
from operator  import itemgetter, attrgetter

import parcoursprog

logging.basicConfig()

#General parameters
hysteresisInertia          = 0.01
percentageAccuracy         = 0.02
screenSize                 = int(1080*0.8)
percentageAccuracyInPixels = percentageAccuracy * screenSize
maxDepth                   = 0xffffffff

def printDocAndExit():
    """
    Print the documentation of how to use the script and exit the program.
    """
    print("Run ./benchmark.py [data_directory] [--output JsonOutput]")
    print("                   [-h] [--help]")
    print("data_directory: the data directory containing the preparsed data binary files that parcoursvis will read. Default: ${pwd}/tmp")
    print("--output: output path to save the benchmark results in a JSON format. Default: output.json")
    print("-h --help: Print this documentation and exit.")
    print("""\nNote that ParcoursVis relies on OpenMP. To influence the number of threads OpenMP relies on,
change the environment variables OMP_NUM_THREADS and OMP_DYNAMIC as required (see also their respective documentation)
          """)

    sys.exit(-1)

class BenchmarkMetrics:
    """
    Stores the data about the benchmark per depth
    The class stores a list of BenchmarkMetrics.IterationData for subsequent data analysis
    """
    class IterationData:
        """
        Metrics store per each iteration
        """
        def __init__(self):
            self.permutations  = 0
            self.badStatistics = 0
            self.missingNodes  = 0

        def isStable(self, checkTopology):
            if checkTopology:
                return self.stableWithTopology
            return self.stableWithoutTopology

        stableWithTopology    = property(lambda self: self.permutations == 0 and self.badStatistics == 0 and self.missingNodes == 0)
        stableWithoutTopology = property(lambda self: self.permutations == 0 and self.badStatistics == 0)

    def __init__(self):
        self._iterations     = []

    def __iter__(self):
        """
        Iterate over the iterations stored as IterationData in this object
        """
        return self._iterations.__iter__()

    def __len__(self):
        """
        The number of iterations stored
        """
        return len(self._iterations)

    def __getitem__(self, x):
        """
        Get the iteration x. 
        return: IterationData
            the IterationData element stored at emplacement x
        """
        return self._iterations[x]

    def insertIteration(self):
        """
        Insert a new IterationData at the end of the list of stored iterations
        return: IterationData
            The new iteration created and append in this class
        """
        self._iterations.append(BenchmarkMetrics.IterationData())
        return self._iterations[-1]

    def getStableAt(self, checkTopology):
        """
        Get at what iteration is the generated tree stable
        checkTopology: boolean
            should we check the topology (i.e., the number of missing Nodes)?
        return: int
            the iteration ID of when the tree can be considered stable

            This function ensures that:
            for x in range(getStableAt(self), len(self)):
                assert self[x].stable
        """
        ret = 0
        for y,x in enumerate(self._iterations[::-1]):
            if not x.isStable(checkTopology):
                ret = len(self._iterations)-y
                break

        for x in range(ret, len(self)):
            assert self[x].isStable(checkTopology)

        return ret

    stableWithTopologyAt    = property(lambda self: self.getStableAt(True))
    stableWithoutTopologyAt = property(lambda self: self.getStableAt(False))

class ProgressiveStats:
    """
    Class to benchmark the progressive algorithm of parcoursvis
    """
    def __init__(self):
        """
        Constructor. Initialize component but does not read the data yet. 
        Use self.processData to process the data

        outputFile: str
            The path to output the result of the algorithm via the "print" function
        """
        self._distributions     = list()
        self._computationTimes  = list()
        self.ctx                = parcoursprog.context()
        self.percentageAccuracy = percentageAccuracy
        self.hysteresisInertia  = hysteresisInertia
        self.chunkSize          = int(1e5)
        self.threshold          = 0

        self.alignSequence      = []
        self.alignSequenceBy    = 0

    def _areFloatsAlmostSimilar(self, f1, f2):
        """
        Are two float values similar at "self.percentageAccuracy"?
        f1, f2: float
            The two values to compare
        output: boolean
            True if f1 and f2 are close to each other, False otherwise.
        """
        return abs(f1-f2) <= self.percentageAccuracy

    def getNodeByID(self, distribution, i):
        """
        Find the node of a given tree by its ID
        distribution : dict
            The dict containing the tree. Should contain "id" (Integer) and an optional dict "children" which has the same dataset structure
            All IDs should be unique (i.e., there should be no duplication)
        i : Integer
            The ID of the node to look for
        return : dict or None
            If found, this function returns the node which has the corresponding ID. Else, it returns None. The node should have the same data structure as "distribution"
        """
        def find_rec(curNode):
            if curNode["id"] == i:
                return curNode
            elif curNode["id"] > i:
                return None #This is a specificity of our graph: Nodes are inserted in bigger order, so child[id] > this[id]

            if "children" in curNode:
                for _,child in curNode["children"].items():
                    value = find_rec(child)
                    if value is not None:
                        return value
            return None

        return find_rec(distribution)
                    
    #The next three methods relies on the same signature because they are meant to be given by parameters
    #to the statistical functions (see print)

    def sortNodesByIDs(self, nodes, parent=None):
        """
        Sort all the received nodes by their "id" from biggest to lowest 
        This will be then compatible with the fact that newly added nodes (and thus non-existing nodes) will be at the beginning
        of the list
        nodes: list of dict
            the nodes of the tree to sort
        parent: dict
            the parent owning the nodes
        output: list of dict
            the nodes sorted
        """
        return sorted(nodes, key = itemgetter("id"))

    def sortNodesByCount(self, nodes, parent=None):
        """
        Sort all the received nodes by their "count" 
        nodes: list of dict
            the nodes of the tree to sort
        parent: dict
            the parent owning the nodes
        output: list of dict
            the nodes sorted
        """
        return sorted(nodes, key = itemgetter("count"))

    def sortNodesByCount_hysteresis(self, nodes, parent):
        """
        Sort all the received nodes by their "count" taking into account the hysteresis. As such, nodes such be part of self._distributions
        nodes: list of dict
            the nodes of the tree to sort. Nodes should be part of one of the self.iterations property (self._distributions)
        parent: dict
            the parent owning the nodes
        output: list of dict
            the nodes sorted taking into account some late effects
        """
        previousNodes  = []
        previousParent = None
        for dist in self._distributions:
            #Search the parent node in the distribution if applicable
            tempNodeParent   = self.getNodeByID(dist, parent["id"])
            if tempNodeParent is None:
                continue

            sortedNodes = list(tempNodeParent["children"].values() if 'children' in tempNodeParent else []) 
            #If first iteration -> sort in the usual way
            if previousParent is None:
                sortedNodes = self.sortNodesByCount(sortedNodes, tempNodeParent) #Always sort by the count value if first iteration

            #Else: Ensure that the new children are within the same organization as the previous children
            else:
                for i in range(len(previousNodes)):
                    #If the IDs are different --> check if the node with the same ID has a value not that far from the previous iteration
                    if previousNodes[i]["id"] != sortedNodes[i]["id"]:
                        for j in range(i, len(sortedNodes)):
                            if sortedNodes[j]["id"] == previousNodes[i]["id"]:
                                #If yes -> move the value upward until it reaches the same position as it was previously
                                #if abs(sortedNodes[j]["count"]   / tempNodeParent["count"] - \
                                #       previousNodes[i]["count"] / previousParent["count"]) <= self.hysteresisInertia:
#                                for k in range(j, i, -1):
#                                    (sortedNodes[k-1], sortedNodes[k]) = (sortedNodes[k], sortedNodes[k-1])
                                sortedNodes[i], sortedNodes[j] = sortedNodes[j], sortedNodes[i]
                                break


                #And check the sorting with an inertia using a bubble sort
                #This "imperfect" sorting ensures the following equation: 
                #forall{i, j} : 0 < i < j < len(sortedNodes) -> weight(sortedNodes[i]) − weight(sortedNodes[j]) ≤ hysteresisInertia
                if len(sortedNodes) > 1:
                    hasPermuted = True
                    while hasPermuted:
                        hasPermuted     = False
                        maxValue        = sortedNodes[0]["count"]
                        lastMaxValueIDx = 0
                        for i in range(0, len(sortedNodes)): #We always restart from 0 because of the inertia condition
                            if maxValue > sortedNodes[i]["count"] and (maxValue - sortedNodes[i]["count"])/tempNodeParent["count"] > self.hysteresisInertia:
                                for j in range(i, lastMaxValueIDx, -1):
                                    (sortedNodes[j], sortedNodes[j-1]) = (sortedNodes[j-1], sortedNodes[j])
                                hasPermuted = True
                            elif maxValue < sortedNodes[i]["count"]:
                                maxValue = sortedNodes[i]["count"]
                                lastMaxValueIDx = i

            previousNodes  = sortedNodes
            previousParent = tempNodeParent

            if tempNodeParent["count"] == parent["count"]: #Check the count --> If same count, then this last iteration is similar (if not) the current one
                break

        return previousNodes

    def getIterationStatistics(self, depth, sortNodesFn):
        """
        Generate the BenchmarkMetrics of this benchmark at a given depth 

        depth: int
            The depth to check for stability. Depth should start at 1, as the level "0" corresponds to the "root" node.
        sortNodesFn: Function (see sortNodesByIDs and sortNodesByCount)
            Which function should be used to sort the children of a particular node?
        output: BenchmarkMetrics
            Return the iteration statistics at depth == depth
            -Whether the nodes of both a given iteration and the whole tree (as processed entirely) possesses the same node
            in the same order (depends on the sorting method)
            -Whether the percentage distribution for all nodes are similar to the distribution of the whole tree (as processed entirely),
            see self.percentageAccuracy.
        """
        benchmarkMetrics = BenchmarkMetrics()
        lastIteration    = self._distributions[-1]

        def countNbNodes_rec(node, curDepth):
            if curDepth == depth:
                return 0

            ret = 1
            if "children" in node:
                for x in node["children"].values():
                    ret += countNbNodes_rec(x, curDepth+1)

            return ret


        #By construction of this algorithm (see the addition of None objects and the counting of permutations), we are dealing with pairs of
        #nodes that have the same IDs
        def iterNode_rec(parentNodeCurIter, totalCurDistribution, parentNodeLastIter, totalLastDistribution, curDepth, iterationData):
            if curDepth == depth:
                return

            assert parentNodeCurIter["id"] == parentNodeLastIter["id"]

            #Get the children. Handle the case where a node is a leaf
            curChildren  = parentNodeCurIter["children"].values()  if 'children' in parentNodeCurIter  else []
            lastChildren = parentNodeLastIter["children"].values() if 'children' in parentNodeLastIter else []

            #Sort the nodes in both cases
            curChildren  = sortNodesFn(curChildren , parentNodeCurIter)
            lastChildren = sortNodesFn(lastChildren, parentNodeLastIter)

            #Reorder the nodes and count the number of permutation done using a "bubble sort" algorithm
            for i in range(len(lastChildren)):
                if i < len(curChildren) and curChildren[i] != None and curChildren[i]["id"] == lastChildren[i]["id"]: #if no permutation -> happy ending
                    continue

                hasPermuted = False
                for j in range(i+1, len(curChildren)): #Else try to find the permutation to do
                    if curChildren[j] == None: 
                        continue

                    if curChildren[j]["id"] == lastChildren[i]["id"]:
                        curChildren[i], curChildren[j] = curChildren[j], curChildren[i] #Permut
                        hasPermuted = True
                        iterationData.permutations+=1
                        break

                if not hasPermuted: #No permutation? It means we did not find the ID -> create a "None" node
                    iterationData.missingNodes += countNbNodes_rec(lastChildren[i], curDepth)

                    curChildren.insert(i, None)

            assert len(curChildren) == len(lastChildren)

            for curNode, lastNode in zip(curChildren, lastChildren):
                curCount = 0 if curNode is None else curNode["count"]

                #Check the statistics. If curNode is None, it means that we did not find the corresponding node. We can, however, count the statistics as if this node
                #Exists for every iteration with a count of 0
                if not self._areFloatsAlmostSimilar(curCount         /totalCurDistribution,\
                                                    lastNode["count"]/totalLastDistribution):
                    iterationData.badStatistics += 1
                if curNode != None:
                    iterNode_rec(curNode, totalCurDistribution, lastNode, totalLastDistribution, curDepth+1, iterationData)

        for i in range(len(self._distributions)-1):
            distribution = self._distributions[i]
            iterNode_rec(distribution, distribution["count"], lastIteration, lastIteration["count"], 0, benchmarkMetrics.insertIteration())

        return benchmarkMetrics

    def getNbIterations(self):
        """
        return : Integer
            The number of iterations of the overall progressive algorithm
        """
        return len(self._distributions)

    def processData(self, dataDirectory):
        """
        Launch the progressive algorithm until its end by reading the data of "dataDirectory".
        This method saves all the needed data for later analysis
        dataDirectory: str
            The path containing the data for parcoursvis
        """
        print(f"# using data from directory {dataDirectory}")
        self.ctx.load(dataDirectory)
        self.ctx.align_sequence       = self.alignSequence
        self.ctx.align_sequence_by    = self.alignSequenceBy
        self.ctx.apply_filter         = False
        self.ctx.prune_threshold      = self.threshold
        self.ctx.chunk_size           = self.chunkSize
        self.ctx.max_depth            = maxDepth
        self.ctx.progressive_strategy = parcoursprog.ProgressiveStrategy.CHUNK_PATIENTS
        isFirst = True

        while not self.ctx.process_done():
            startTime = time.time()
            self.ctx.process_next(isFirst)
            endTime = time.time()
            isFirst = False
            self._computationTimes.append(endTime-startTime)
            self._distributions.append(self.ctx.tree())

    def print(self, jsonOutput):
        """
        Print both in the standard output and in self.outputFile the results of the analysis.
        jsonOutput : dict
            The JSON output data structure. Should contain the keys "ProgressiveProg" and "ProgressiveStats". See "initOutputDict" for more information
        """

        performance = {"iterations": [], "stats": [], "totalTime": self.totalComputationTime, "alignSequence": [self.ctx.event_name(x) for x in self.ctx.align_sequence], "alignSequenceBy": self.ctx.align_sequence_by, "chunk_size": self.ctx.chunk_size, "threshold": self.ctx.prune_threshold}
        for i, time in enumerate(self._computationTimes):
            performance["iterations"].append({'time': time, 'nbPatients': self._distributions[-1]["nb_processed"]})
            print(f"# iteration {i}: {(time)*1000:.2f}ms")
        print(f"# computation time of process_progressive: {(self.totalComputationTime)*1000:.2f}ms, number of iteration: {len(self._computationTimes)}")

        for sortNodes, sortMethodStr in zip((self.sortNodesByIDs, self.sortNodesByCount, self.sortNodesByCount_hysteresis), ("IDs", "Count", "Hysteresis")):
            print(f"# Check statistics by sorting nodes by {sortMethodStr}")
            for depth in range(5):
                iterData = self.getIterationStatistics(depth+1, sortNodes)
                performance["stats"].append({"sortingMethod": sortMethodStr, "depth": depth, "stableWithTopologyAtIter": iterData.stableWithTopologyAt, "stableWithoutTopologyAtIter": iterData.stableWithoutTopologyAt,\
                                             "data": [{"permutations": x.permutations, "badStatistics": x.badStatistics, "missingNodes": x.missingNodes} for x in iterData]})
                print(f"# Iteration stable, for depth {depth} at: {iterData.stableWithTopologyAt}")
            print("\n")
        jsonOutput["Progressive"].append(performance)

    iterations           = property(lambda self : self._distributions)
    totalComputationTime = property(lambda self : sum(self._computationTimes))

def processAll_cache(dataDirectory):
    """
    Process the dataset given as an argument to force the system to cache the different files
    dataDirectory : str
        The absolute or relative path to the directory containing the files data.bin, data.map, and indi.bin containing the overall dataset
    """
    ctx = parcoursprog.context()
    ctx.load(dataDirectory)
    ctx.apply_filter = False
    ctx.process_all()

    return ctx

def printProcessAll(ctx, computationTime, output):
    tree = ctx.tree()
    output["NonProgressive"].append({"time": computationTime, "alignSequence": [ctx.event_name(x) for x in ctx.align_sequence], "alignSequenceBy": ctx.align_sequence_by,\
                                     "chunk_size": ctx.chunk_size, "threshold": ctx.prune_threshold, "nbPatients": tree['count'],\
                                     "nbLowLevel": ctx.get_nb_low_level_events(), "nbHighLevel": sum(tree['subcount'].values()),\
                                     "nbPathways": ctx.get_nb_distinct_pathways(), "nbNodes": ctx.get_nb_useful_nodes()})
    print(f"# Information for ProcessAll:")
    print(f"# computation time of process_all: {(computationTime)*1000:.2f}ms")
    print(f"# number of patients: {tree['count']}")
    print(f"# number of low_level events: {ctx.get_nb_low_level_events()}")
    print(f"# number of high_level events: {sum(tree['subcount'].values())}")
    print(f"# number of distinct pathways: {ctx.get_nb_distinct_pathways()}")
    print(f"# number of displayed nodes: {ctx.get_nb_useful_nodes()}")
    print("\n")

def benchmarkNonAlign(dataDirectory, output):
    """
    Benchmark the ParcoursVis' different algorithms for non-aligned views
    dataDirectory : str
        The absolute or relative path to the directory containing the files data.bin, data.map, and indi.bin containing the overall dataset
    output: dict
        The JSON dict output of the script. See initOutputDict
    """
    print("# START THE BENCHMARK FOR NON-ALIGNED VIEWS\n")

    #Start to benchmark... 
    for chunkSize in [100000, 150000, 200000, 250000]:
        for threshold in [0, 25, 50]:
            #...The non-progressive algorithm
            ctx = parcoursprog.context()
            ctx.load(dataDirectory)
            ctx.apply_filter = False
            ctx.chunk_size = chunkSize
            ctx.prune_threshold = threshold
            ctx.max_depth = maxDepth
            startTime = time.time()
            ctx.process_all()
            endTime   = time.time()
            printProcessAll(ctx, endTime-startTime, output)

            print("\n# Information for ProcessProgressive:")
            #...The progressive algorithm
            progressiveStats = ProgressiveStats()
            progressiveStats.chunkSize = chunkSize
            progressiveStats.threshold = threshold
            progressiveStats.processData(dataDirectory)
            progressiveStats.print(output)
            print("\n")


def benchmarkAligned(dataDirectory, alignSequence, alignBy, output):
    """
    Benchmark the ParcoursVis' different algorithms for aligned views
    dataDirectory : str
        The absolute or relative path to the directory containing the files data.bin, data.map, and indi.bin containing the overall dataset
    alignSequence : list of str
        The sequence to be aligned with. The string should match the names of the events ParcoursVis handles.
    alignBy : Integer
        Set the "align_sequence_by" value of ParcoursVis.
    """
    print("# START THE BENCHMARK FOR ALIGNED VIEWS.\n")
    print(f"Align parameters: {alignSequence}:{alignBy}")

    #Initialize a default context to retrieve all the types of events ParcoursVis handles
    ctx = parcoursprog.context()
    ctx.load(dataDirectory)
    types = ctx.valid_type_combination
    for x in alignSequence:
        if not x in types:
            raise ValueError(f"Could not find event named {x}.")
    alignSequenceIDs = [types[x] for x in alignSequence]

    #Process all the data at once to get basic information
    ctx.apply_filter      = False
    print(alignSequenceIDs)
    ctx.align_sequence    = alignSequenceIDs
    ctx.align_sequence_by = alignBy
    startTime = time.time()
    ctx.process_all()
    endTime   = time.time()
    printProcessAll(ctx, endTime-startTime, output)

    #Start to benchmark the progressive algorithm
    progressiveStats = ProgressiveStats()
    progressiveStats.alignSequence   = alignSequenceIDs
    progressiveStats.alignSequenceBy = alignBy
    progressiveStats.processData(dataDirectory)
    print("\n# Information for ProcessProgressive:")
    progressiveStats.print(outputData)

def initOutputDict():
    data = {}
    data["GeneralParameters"] = {"hysteresisInertia": hysteresisInertia, "percentageAccuracy": percentageAccuracy, \
                                 "screenSize": screenSize, "percentageAccuracyInPixels": percentageAccuracyInPixels}
    data["NonProgressive"]   = []
    data["Progressive"]      = []

    return data

if __name__ == '__main__':
    argv          = sys.argv[1:]
    dataDirectory = "tmp"
    files         = []
    outputFile    = os.getcwd() + "/output.json"
    outputData    = initOutputDict()

    #Read argv
    i = 0
    while i < len(argv):
        arg = argv[i]
        if i == 0 and not (arg.startswith("--") or arg.startswith("-")):
            dataDirectory = arg
            i+=1
        else:
            while i < len(argv):
                arg = argv[i]

                if arg == "-h" or arg == "--help":
                    printDocAndExit()

                elif arg == "--output":
                    if i < len(argv)-1:
                        if argv[i+1][0] == '-':
                            print(f"Missing JSON file path value to the '--output' parameter. {argv[i+1]} is not a correct value. Exiting.")
                            sys.exit(-1)

                        outputFile = argv[i+1]
                        i+=1
                    else:
                        print("Missing JSON file path value to the '--output' parameter. Exiting.")
                        sys.exit(-1)
                else:
                    print(f"Unknown parameter {arg}")
                    printDocAndExit()
                i+=1

    #Check that the data exists
    assert os.path.isfile(os.path.join(dataDirectory, "data.bin"))
    assert os.path.isfile(os.path.join(dataDirectory, "data.map"))
    assert os.path.isfile(os.path.join(dataDirectory, "indi.bin"))

    #Fill the file cache system linux systems have... Otherwise, the second benchmarking will be faster than the first one
    print("Start to parse twice the dataset to cache the memmap file...")
    for i in range(2):
        processAll_cache(dataDirectory)

    #Start the benchmark for non-aligned and aligned trees
    benchmarkNonAlign(dataDirectory, outputData)
    for align in ["Zone_1", "Zone_2"]:
        benchmarkAligned(dataDirectory, [align], 0, outputData)

    #Save the data into the file "outputFile" in a JSON format
    with open(outputFile, 'w') as f:
        json.dump(outputData, f, indent=2)
