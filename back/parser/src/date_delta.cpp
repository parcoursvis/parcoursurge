// C++ program two find number of 
// days between two given dates
#include <iostream>
#include "date_delta.hpp"

using namespace std;

const time_t seconds_per_day = (24*60*60);

int date_to_days(int year, int month, int day) {
  // Use library utilities to be sure
  // sec,min,hour,mday,mon,year,wday,yday,idst
  struct tm t = {0, 0, 11, day, month-1, year-1900, 0, 0, 0, 0, 0};
  // t.tm_hour = 11;
  // t.tm_mday = day;
  // t.tm_mon = month-1;
  // t.tm_year = year-1900;

  return int(mktime(&t)/seconds_per_day); // use EPOCH-based encoding
}

bool days_to_date(int days, int& year, int& month, int& day) {
  time_t time = days * seconds_per_day;
  struct tm t;
  bool ok = (gmtime_r(&time, &t) == &t);
  if (ok) {
    day = t.tm_mday;
    month = t.tm_mon+1;
    year = t.tm_year+1900;
  }
  return ok;
}

