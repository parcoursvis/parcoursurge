#include <iostream>
#include <gtest/gtest.h>
#include "pparser.hpp"

class GraphTest : public testing::Test {
    protected:
        GraphTest() {}

        void SetUp() override {
            Node& root = graph.nodes[0];

            //Structure
            root.count = 30;
            root.subcount[1] = 10;
            root.subcount[2] = 20;
            root.children[1] = graph.id(graph.new_node());
            graph.nodes[root.children[1]].count = 10;
            graph.nodes[root.children[1]].subcount[1] = 10;
                graph.nodes[root.children[1]].children[3] = graph.id(graph.new_node());
                graph.nodes[graph.nodes[root.children[1]].children[3]].count = 4;
                graph.nodes[graph.nodes[root.children[1]].children[3]].subcount[3] = 4;
                graph.nodes[root.children[1]].subcount[3] = 4;

                graph.nodes[root.children[1]].children[4] = graph.id(graph.new_node());
                graph.nodes[graph.nodes[root.children[1]].children[4]].count = 1;
                graph.nodes[graph.nodes[root.children[1]].children[4]].subcount[4] = 1;
                graph.nodes[root.children[1]].subcount[4] = 1;
            
            root.children[2] = graph.id(graph.new_node());
            graph.nodes[root.children[2]].count = 20;
            graph.nodes[root.children[2]].subcount[2] = 30;
                graph.nodes[root.children[2]].children[2] = graph.id(graph.new_node());
                graph.nodes[graph.nodes[root.children[2]].children[2]].count = 10;
                graph.nodes[graph.nodes[root.children[2]].children[2]].subcount[2] = 10;


            //Age and duration
            for(uint32_t i = 0; i < root.count; i++) {
                root.ages.compute(i);
                root.durations.add(100+i, true);
            }
            for(uint32_t i = 0; i < 10; i++) {
                graph.nodes[root.children[1]].ages.compute(i);
                graph.nodes[root.children[1]].durations.add(100+i, true);
            }
            for(uint32_t i = 0; i < 4; i++) {
                graph.nodes[graph.nodes[root.children[1]].children[3]].ages.compute(i);
                graph.nodes[graph.nodes[root.children[1]].children[3]].durations.add(100+i, true);
            }

            graph.nodes[graph.nodes[root.children[1]].children[4]].ages.compute(4);
            graph.nodes[graph.nodes[root.children[1]].children[4]].durations.add(100+4, true);

            for(uint32_t i = 0; i < 20; i++) {
                graph.nodes[root.children[2]].ages.compute(10+i);
                graph.nodes[root.children[2]].durations.add(100+10+i, true);
            }
            for(uint32_t i = 0; i < 10; i++) {
                graph.nodes[graph.nodes[root.children[2]].children[2]].ages.compute(10+i);
                graph.nodes[graph.nodes[root.children[2]].children[2]].durations.add(100+10+i, true);
            }


            //State of the graph:
            //(root):ID 0 --> 1:ID 1 (10) --> 3: ID 2 (4)
            //                            --> 4: ID 3 (1) --> 5: ID 6 (1)
            //            --> 2:ID 4 (20) --> 2: ID 5 (10)
            //Ages:        0 -> 29  (following the construction order)
            //Durations: 100 -> 129 (following the construction order)
        }

        Graph graph;
};

TEST_F(GraphTest, merge) {
    Graph g = graph;
    g.nodes[3].children[5] = g.id(g.new_node());
    g.nodes[6].count = 1;
    g.nodes[0].subcount[5] = 1;
    g.nodes[1].subcount[5] = 1;
    g.nodes[3].subcount[5] = 1;
    g.nodes[6].subcount[5] = 1;
    g.nodes[6].ages.compute(g.nodes[3].ages[0]);
    g.nodes[6].durations.add(g.nodes[3].durations[0], true);
    g.nodes[0].indi_diseases[0].add(8);
    g.nodes[4].indi_diseases[0].add(8);

    //structure of the graph:
    //(root):ID 0 --> 1:ID 1 (10) --> 3: ID 2 (4)
    //                            --> 4: ID 3 (1) --> 5: ID 6 (1)
    //            --> 2:ID 4 (20) --> 2: ID 5 (10)
    graph.nodes[0].indi_diseases[0].add(10);
    graph.nodes[4].indi_diseases[0].add(10);
    graph.nodes[5].indi_diseases[0].add(10);
    graph += g;

    Node& root = graph.nodes[0];


    //Check structure and counting
    EXPECT_EQ(root.count, 60);
    EXPECT_EQ(root.subcount[1], 20);
    EXPECT_EQ(root.subcount[2], 40);
    EXPECT_EQ(root.subcount[5], 1);
    EXPECT_EQ(graph.nodes[root.children[1]].subcount[1], 20);
    EXPECT_EQ(graph.nodes[root.children[1]].subcount[3], 8);
    EXPECT_EQ(graph.nodes[root.children[1]].subcount[4], 2);
    EXPECT_EQ(graph.nodes[root.children[1]].subcount[5], 1);
    EXPECT_EQ(graph.nodes[root.children[1]].count, 20);
    EXPECT_EQ(graph.nodes[graph.nodes[root.children[1]].children[3]].count, 8);
    EXPECT_EQ(graph.nodes[graph.nodes[root.children[1]].children[3]].subcount[3], 8);
    EXPECT_EQ(graph.nodes[graph.nodes[root.children[1]].children[4]].count, 2);
    EXPECT_EQ(graph.nodes[graph.nodes[root.children[1]].children[4]].subcount[4], 2);
    EXPECT_EQ(graph.nodes[graph.nodes[root.children[1]].children[4]].subcount[5], 1);
    EXPECT_EQ(graph.nodes[graph.nodes[graph.nodes[root.children[1]].children[4]].children[5]].count, 1);
    EXPECT_EQ(graph.nodes[graph.nodes[graph.nodes[root.children[1]].children[4]].children[5]].subcount[5], 1);

    EXPECT_EQ(graph.nodes[root.children[2]].count, 40);
    EXPECT_EQ(graph.nodes[root.children[2]].subcount[2], 60);
    EXPECT_EQ(graph.nodes[graph.nodes[root.children[2]].children[2]].count, 20);
    EXPECT_EQ(graph.nodes[graph.nodes[root.children[2]].children[2]].subcount[2], 20);

    //Check ages and durations
    auto checkAgesAndDurations = [](const Node& node, uint64_t start) {
        EXPECT_EQ(node.ages.count,       node.count);
        EXPECT_EQ(node.durations.size(), node.count);
        for(uint32_t i = 0; i < node.count/2; i++) {
            EXPECT_EQ(node.durations[2*i],   100+i+start);
            EXPECT_EQ(node.durations[2*i+1], 100+i+start); //Lists are sorted
        }
    };

    checkAgesAndDurations(root, 0);
    checkAgesAndDurations(graph.nodes[root.children[1]], 0);
    checkAgesAndDurations(graph.nodes[graph.nodes[root.children[1]].children[3]], 0);
    checkAgesAndDurations(graph.nodes[graph.nodes[root.children[1]].children[4]], 4);
    checkAgesAndDurations(graph.nodes[graph.nodes[graph.nodes[root.children[1]].children[4]].children[5]], 4);
    checkAgesAndDurations(graph.nodes[root.children[2]], 10);
    checkAgesAndDurations(graph.nodes[graph.nodes[root.children[2]].children[2]], 10);

    //Check diseases
    EXPECT_EQ(root.indi_diseases[0].cardinality(), 2);
    EXPECT_TRUE(root.indi_diseases[0].contains(10));
    EXPECT_TRUE(root.indi_diseases[0].contains(8));
    EXPECT_EQ  (graph.nodes[root.children[2]].indi_diseases[0].cardinality(), 2);
    EXPECT_TRUE(graph.nodes[root.children[2]].indi_diseases[0].contains(10));
    EXPECT_TRUE(graph.nodes[root.children[2]].indi_diseases[0].contains(8));
    EXPECT_EQ  (graph.nodes[graph.nodes[root.children[2]].children[2]].indi_diseases[0].cardinality(), 1);
    EXPECT_TRUE(graph.nodes[graph.nodes[root.children[2]].children[2]].indi_diseases[0].contains(10));
}

TEST_F(GraphTest, clear) {
    graph.clear();
    EXPECT_EQ(graph.nodes.size(), 1);
    EXPECT_EQ(graph.nodes[0].children.size(), 0);
    EXPECT_EQ(graph.nodes[0].subcount.size(), 0);
}
