#ifndef DATE_DELTA_HPP
#define DATE_DELTA_HPP

int date_to_days(int year, int month, int day);
bool days_to_date(int days, int& year, int& month, int& day);

#endif
