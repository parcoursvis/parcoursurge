#ifndef INFO_HPP
#define INFO_HPP

#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include <cstdint>

struct Info {
  uint32_t name;
  uint32_t drug;
  uint64_t seconds;
  uint8_t  type;
  uint8_t  age;

  Info(uint32_t _name = 0, uint32_t _drug = 0, uint64_t _seconds = 0, uint32_t _type = 0, uint32_t _age = 0) :
      name(_name), drug(_drug), seconds(_seconds), type(_type), age(_age) {}
  Info(const Info &info) {*this = info;}
  Info& operator=(const Info& info)
  {
    if(this != &info) {
      name    = info.name;
      drug    = info.drug;
      seconds = info.seconds;
      type    = info.type;
      age     = info.age;
    }
    return *this;
  }
  void clear() { name = 0; drug = 0; seconds = 0; type = 0; age = 0; }
};
const Info INFO_V1 = { 0x12, 0x34, 0, 1, 0x9A };

inline bool operator == (const Info& a, const Info& b) {
  return 
    a.name    == b.name    &&
    a.drug    == b.drug    &&
    a.seconds == b.seconds &&
    a.type    == b.type    &&
    a.age     == b.age;
}

inline bool operator != (const Info& a, const Info& b) {
  return ! (a == b);
}


struct IndiInfo {
  uint32_t name;
  uint32_t start_index;
  uint32_t end_index;  
  uint8_t  start_age;
  uint8_t  diseases; 
  void clear() { name = 0; start_age = 0; end_index = 0; diseases = 0; }
};
const IndiInfo INDI_INFO_V1 = { 0xA9, 0x87, 0, 1, 0x21 };

inline bool operator == (const IndiInfo& a, const IndiInfo& b) {
  return a.name == b.name &&
    a.start_index == b.start_index &&
    a.end_index == b.end_index &&
    a.start_age == b.start_age &&
    a.diseases == b.diseases;
}

inline bool operator != (const IndiInfo& a, const IndiInfo& b) {
  return ! (a == b);
}


inline std::istream& read_dict(std::istream& fin, std::vector<std::string>& dict) {
  std::string val;
  std::getline(fin, val);
  if (! fin) return fin;  
  int size = std::stoi(val);
  dict.resize(size);
  for (auto& it : dict) {
    std::getline(fin, it);
  }

  return fin;
}

inline std::ostream& write_dict(std::ostream& out, const std::vector<std::string>& dict) {
  out << dict.size() << std::endl;
  for (auto& val : dict)
    out << val << std::endl;
  return out;
}

inline std::istream& read_dicts(std::istream& fin, 
                                std::vector<std::string>& types,
                                std::vector<std::string>& drugs,
                                std::vector<std::string>& names,
                                std::vector<std::string>& diseases) {
  if (! read_dict(fin, types)) return fin;
  if (! read_dict(fin, drugs)) return fin;
  if (! read_dict(fin, names)) return fin;
  return read_dict(fin, diseases);
}

inline std::ostream& write_dicts(std::ostream& out, 
                                 const std::vector<std::string>& types,
                                 const std::vector<std::string>& drugs,
                                 const std::vector<std::string>& names,
                                 const std::vector<std::string>& diseases) {
  if (! write_dict(out, types)) return out;
  if (! write_dict(out, drugs)) return out;
  if (! write_dict(out, names)) return out;
  return write_dict(out, diseases);
}
#endif
