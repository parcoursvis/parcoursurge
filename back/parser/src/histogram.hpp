#ifndef HISTOGRAM_HPP
#define HISTOGRAM_HPP

#include <vector>
#include <list>
#include <iostream>
#include <sstream>

/** \brief  Basic structure for histogram
 *
 * @tparam T the granularity type of the histogram. Should be a native type number (char, int, float, double, etc.)*/
template <typename T>
struct histogram {
    std::vector<unsigned> bins;
    T min_value;
    T max_value;
    unsigned long count;
    T cumsum; 

    /** \brief  Constructor
     *
     * \param size the number of bins
     * \param min the minimum values of the histogram
     * \param max the maximum values of the histogram */
    histogram(std::size_t size, const T& min, const T& max) 
        : bins(size, 0), 
          min_value(min),
          max_value(max),
          count (0),
          cumsum(0) { }

    /** \brief  Constructor, initialize the histogram with default values
     * \param size the number of bins
     * \param data the data to (1) use to set the range, and (2) to populate the histogram from */
    histogram(std::size_t size, const std::vector<T>& data) 
        : bins(size, 0), 
          min_value(0),
          max_value(0),
          count (0),
          cumsum(0) 
    {
        set_range(data);
        compute(data);
    }

    histogram(histogram<T>&& mvt) = default;
    histogram(const histogram<T>& cpy) = default;
    histogram<T>& operator=(const histogram<T>& cpy) = default;

    /** \brief  Add the value of an histogram to this one
     *  \param other the other histogram, which should have the same number of bins and bounds
     *  \return *this */
    histogram& operator += (const histogram& other) {
        if (not (other.bins.size() == bins.size()
                 && other.min_value == min_value
                 && other.max_value == max_value))
            throw std::invalid_argument("Cannot add histograms of different sizes or bounds");
        count += other.count;
        cumsum += other.cumsum;
        for (size_t i = 0; i < size(); ++i)
            bins[i] += other.bins[i];
        return *this;
    }


    /** \brief  Return the value at the bin "index"
     * \param index the index to look at. See std::vector::operator[] for potential errors
     * \return  the value at bin "index" */
    const unsigned& operator[](int index) const { return bins[index]; }

    /** \brief  Get the number of bins this histogram has
     * \return  the number of bins */
    std::size_t size() const { return bins.size(); }

    /** \brief  Clear the histogram: Reset all the bins to 0 */
    void clear() {
        if (count) {
            std::fill(bins.begin(), bins.end(), 0);
            count = 0;
            cumsum = 0;
        }
    }

    /**
     * \brief  Set the range of the histogram. If min > max, the range is inverted and the function returns false
     *
     * \param min the minimum limit of the histogram.
     * \param max the maximum limit of the histogram.
     * \return true if min < max (as normal), false otherwise. This can be used to check for potential errors
     */
    bool set_range(const T& min, const T& max) {
        clear();
        if(min <= max) {
            min_value = min;
            max_value = max;
            return true;
        }
        else {
            min_value = max;
            max_value = min;
            return false;
        }
    }

    /** \brief  Set the range of the histogram using different values stored in an array
     * \param data the array of data from which the range should be set. This range can then be used in Histogram::compute */
    void set_range(const std::vector<T>& data) {
        clear();
        auto it = data.begin();
        if (it == data.end()) return;
        min_value = *it;
        max_value = *it;
        for (; it != data.end(); ++it) {
            if (*it < min_value)
                min_value = *it;
            else if (*it > max_value)
                max_value = *it;
        }
    }

    /** \brief  Get the index of value "x" inside the binning
     * \param x the value to evaluate
     * \param output[out] the output value if x is within the histogram range
     * \return   true if x is within the histogram range, false otherwise */
    bool get_bin_index_of(T x, unsigned& output) {
        T range = max_value - min_value;
        unsigned size = bins.size();

        if (x < min_value || x > max_value) // Drop value
            return false;

        unsigned index = (x - min_value) * size / range;
        if (index < 0) // Should not happen
            return false;
        else if (std::size_t(index) >= size) {
            if (x == max_value) // Special case where the binning is weird
                index = size - 1;
            else // Should not happen
                return false;
        }

        output = index;
        return true;
    }

    /** \brief  Add simultaneously different values in the histogram. This function will determine in which bin each data point belongs and increments the resulting bins
     * \param data the vector of data to add */
    void
    compute(const std::vector<T>& data) {
        T range = max_value - min_value;
        unsigned size = bins.size();
        if (size == 0 || range == 0)
            return;
        for (const auto& x : data) {
            compute(x);
        }
    }

    /** \brief  Add simultaneously different values in the histogram. This function will determine in which bin each data point belongs and increments the resulting bins
     * \param data the list of data to add */
    void
    compute(const std::list<T>& data) {
        T range = max_value - min_value;
        unsigned size = bins.size();
        if (size == 0 || range == 0)
            return;
        for (const auto& x : data) {
            compute(x);
        }
    }

    void compute(const T& x) {
        T range = max_value - min_value;
        unsigned size = bins.size();
        if (size == 0 || range == 0)
            return;

        if (x < min_value || x > max_value)
            return;
        // particular case for last the bin
        unsigned index = (x == max_value)
                             ? size - 1
                             : unsigned((x - min_value) * size / range);

        if (index < 0 || index > size) // should not happen
            return;
        count++;
        bins[index]++;
        cumsum += x;

    }

    /** \brief  Determine the median of the histogram. This function returns the
     * "midpoint" of the bin that contains the histogram \return  the
     * approximate median. If min() == max(), return min(). */
    T median_old() const {
        T range = max_value - min_value;
        if (range == 0 || count == 0)
            return min_value;

        size_t size = bins.size();
        unsigned long cum = 0;
        for (size_t i = 0; i < size; i++) {
            unsigned bin = bins[i];
            if (bin == 0)
                continue;
            cum += bin;
            if (2 * cum > count) {
                double median =
                    min_value + double(range) / size * (i + 0.5); // half bar

                return T(median);
            }
        }
        return mean(); // should not happen
    }

    /** \brief  Determine the median of the histogram. 
     * \return  the approximate median. If min() == max(), return min(). */
    T median() const {
        T range = max_value - min_value;
        if (range == 0 || count == 0)
            return min_value;

        size_t size = bins.size();
        unsigned long cum = 0;
        for (size_t i = 0; i < size; i++) {
            unsigned bin = bins[i];
            if (bin == 0)
                continue;
            cum += bin;
            if (2 * cum >= count) {
                // https://math.stackexchange.com/questions/2591946/how-to-find-median-from-a-histogram
                double w = double(range) / size;
                double l = min_value + w * i;
                double F = cum - bin;
                double median = l + w * ((double(count) / 2.0) - F) / bin;

                return T(median);
            }
        }
        return mean(); // should not happen
    }

    /** \brief  Gives the exact mean of the histogram
     * \return the mean of the histogram */
    T mean() const { return count == 0 ? 0 : cumsum / count; }

    /** \brief  Gives the maximum value of the histogram as determined by its range
     * \return   the maximum value */
    T max() const { return max_value; }

    /** \brief  Gives the minimum value of the histogram as determined by its range
     * \return   the minimum value*/
    T min() const { return min_value; }

    /** \brief Print, in a JSON format, the histogram data
     * \param out the output stream to print into
     * \return the parameter "out" */
    std::ostream &
    print(std::ostream &out) {
        out << "{" << std::endl;
        out << "   \"range\": [" << min_value << ", " << max_value << "],"
            << std::endl;
        out << "   \"bins\": [" << bins[0];
        for (size_t i = 1; i < bins.size(); ++i) {
            out << ", " << bins[i];
        }
        out << "]," << std::endl;
        out << "   \"median\": " << median() << "," << std::endl;
        out << "   \"mean\": " << mean() << std::endl << "}" << std::endl;
        return out;
    }

    /** \brief Return a string of the histogram data in a JSON format
     * \return the JSON data*/
    std::string
    str() {
        std::ostringstream out;
        print(out);
        return out.str();
    }
};

#endif
