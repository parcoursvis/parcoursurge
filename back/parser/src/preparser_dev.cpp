#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <sys/stat.h>
#include <string.h>
#include "info.hpp"

using namespace std;

vector<string> names, drugs;
vector<string> diseases{"Diabete", "Hypertension"};
vector<string> types{"fivealpha", "alphabloc", "phyto"};
unordered_map<string, unsigned> type_map, drug_map, disease_map;

vector<IndiInfo> individuals;
unordered_map<string, unsigned> indi_age;

const int MapFindThreshold = 10;

vector<string> split(const string& str, const string& delim) {
	vector<string> res;
	if("" == str) return res;
	char * strs = new char[str.length() + 1];
	strcpy(strs, str.c_str()); 
 
	char * d = new char[delim.length() + 1];
	strcpy(d, delim.c_str());
 
	char *p = strtok(strs, d);
	while(p) {
		string s = p;
		res.push_back(s);
		p = strtok(NULL, d);
	}
 
	return res;
}

unsigned
value_of(const string& val, 
         vector<string>& values, unordered_map<string, unsigned>& vmap) {
    if (values.size() < MapFindThreshold) {
        for (size_t i = 0; i < values.size(); ++i) {
            if (values[i] == val) return i;
        }
    } else {
        auto it = vmap.find(val);
        if (it != vmap.end())
            return it->second;
    }
    size_t value = values.size();
    values.push_back(val);
    vmap[val] = value;
    return value;
}

struct CSVRow : vector<string> {
    istream& readline(istream& str) {
        string line;
        getline(str, line);
        if (line.size() != 0 && line[line.size()-1]=='\r')
            line.resize(line.size()-1);
        istringstream lineStream(line);
        string cell;
        clear();
        while(getline(lineStream, cell, ',')) {
            push_back(cell);
        }
        if (!lineStream && cell.empty())
            push_back(cell);
        return str;
    }
};

int preparse(istream& fin, ostream& fout, ostream& indiout) {
    if (value_of("", drugs, drug_map) != 0) {
        cerr << "No drugs is not index 0" << endl;
    }
    CSVRow row;
    // ignore first CSV line
    if (! row.readline(fin) || row.size() != 5) {
        cerr << "Error reading CSV file " << endl;
        return 1;
    }
    IndiInfo indi = {};
    bool IndiErr = false;
    string lastname;
    
    for (int counter = 0; fin; ) {
        if (counter && counter % 1000000 == 0) {
            cerr << counter << " lines have been parsed." << endl;
        }
        try {
            Info info;

            row.readline(fin);
            if (row.size() != 5) {
                if (! fin) break;
                cerr << "Incomplete line " << counter << endl;
                continue;
            }

            string name = row[0];
            int age = (int)indi_age[name];
            if (age > 150) {
//                cerr << "Illegal age value for line #" << counter << " with age of " << age << endl;
                IndiErr = true;
                continue;
            }

            if (lastname != name) {
                lastname = name;
                if (counter != 0 && !IndiErr) {
                    names.push_back(name);
                    indiout.write((const char*)&indi, sizeof(indi));
                }
                IndiErr = false;
                indi.name = (uint32_t)names.size()-1;
                indi.start_index = counter;
                indi.start_age = (uint8_t)age;
            }
            indi.end_index = counter+1;
            info.name = indi.name;
            info.age = (uint8_t)age;
            string type = row[1];
            info.type = (uint8_t)value_of(type, types, type_map);
            int year, month, day;
            char delim;
            istringstream is(row[2]);
            is >> year >> delim >> month >> delim >> day;
            if (! is  || delim != '-') {
                cerr << "Error reading date as year-month-day" << endl;
                IndiErr = true;
                continue;
            }
            int days = date_to_days_fast(year, month, day);
            if (days < 0 || days > 65535) {
              cerr << "Cannot encode date " << row[3] << " with 16 bits, ignored" << endl;
              days = 0;
            }
            info.days = (uint16_t)days;
            
            string drug = row[3];
            info.drug = (uint32_t)value_of(drug, drugs, drug_map);
            auto disease = split(row[4], " ");
            int disease_status = 0;
            for (auto d: disease) {
                disease_status += (1 << value_of(d, diseases, disease_map));
            }
            indi.diseases = (uint8_t)disease_status;
            fout.write((const char*)&info, sizeof(Info));
            counter++;
        }
        catch(const logic_error& e) {
            cerr << "Error reading line " << counter << endl;
            IndiErr = true;
        }
    }
    if (indi.end_index != 0) {
        indiout.write((const char*)&indi, sizeof(indi));
    }
    return 0;
}

int indiparse(istream& fin) {
    CSVRow row;
    // ignore first CSV line
    if (! row.readline(fin) || row.size() != 5) {
        cerr << "Error reading CSV file " << endl;
        return 1;
    }
    
    for (int counter = 0; fin; ) {
        if (counter && counter % 1000000 == 0) {
            cerr << counter << " lines have been parsed." << endl;
        }
        try {
            row.readline(fin);
            if (row.size() != 5) {
                if (! fin) break;
                cerr << "Incomplete line " << counter << endl;
                continue;
            }

            string name = row[0];
            int age = (int)stof(row[1]);
            if (indi_age.find(name) == indi_age.end()) {
                indi_age[name] = age;
            }
            counter++;
        }
        catch(const logic_error& e) {
            cerr << "Error reading line " << counter << endl;
        }
    }
    return 0; 
}

int main(int argc, char** argv) {
    istream * in = &cin;
    if (argc >= 3) {
        in = new ifstream(argv[2]);
        if (! *in) {
            cerr << "Error opening input file " << argv[1] << endl;
            return 1;
        }
        if (indiparse(*in)) return 1;
    } else {
        cerr << "Two input files are requested." << endl;
    }
    if (argc >= 2) {
        in = new ifstream(argv[1]);
        if (! *in) {
            cerr << "Error opening input file " << argv[1] << endl;
            return 1;
        }
    }
    mkdir("tmp", 0777);
    ofstream fout("tmp/data.bin", ios::binary);
    if (! fout.is_open()) {
        cerr << "Error opening output file" << endl;
        return 1;
    }
    ofstream indiout("tmp/indi.bin", ios::binary);
    if (! indiout.is_open()) {
        cerr << "Error opening indi output file" << endl;
        return 1;
    }
    if (preparse(*in, fout, indiout)) return 1;
    
    ofstream fdict("tmp/data.map", ios::binary);
    if (! fdict.is_open() || ! fout.is_open()) {
        cerr << "Error opening output file(s)" << endl;
        return 1;
    }
    write_dicts(fdict, types, drugs, names, diseases);

    cout << "finish" << endl;
    if (in != &cin)
        delete in;
    return 0;
}


