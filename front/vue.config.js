const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')

const config = {
  publicPath: process.env.VUE_APP_SERVER_PREFIX,
  devServer: {
    host: '0.0.0.0', 
    port: 8080,
  },
  configureWebpack: {
    plugins: [
      new VuetifyLoaderPlugin()
    ],
  },
  pluginOptions: {
    webpackBundleAnalyzer: {
      openAnalyzer: false
    }
  }
};

module.exports = config;
