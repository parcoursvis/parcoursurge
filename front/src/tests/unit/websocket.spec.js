import "regenerator-runtime/runtime.js";
import {connection} from "./utils";

async function wsFactory () {
  let ws = new WebSocket(`ws://${process.env.VUE_APP_IP_VM}:${process.env.VUE_APP_PORT_VM_WS}/ws/`);
  let con = await connection(ws);
  if(con)
    return ws;
  return null;
}

describe("Websocket", () => {
  //Test the connection with the backend server 
  test("connection", async () => {
    let ws = await wsFactory();
    expect(ws).toBeTruthy();
  });

  //Check the return of reset for reset == false
  test("Receive simple node tree -- reset 'false'", (done) => {
    wsFactory().then((ws) => {
      expect(ws).toBeTruthy();
      if(!ws)
        return;

      ws.onmessage = (event) => {
        let resp = JSON.parse(event.data);
        expect(resp.type).toBe("nodetree");
        expect(resp.data.name).toBe("_root");
        expect(resp.data.children.length).toBeGreaterThan(0);
        expect(resp.data.children[0].name).not.toBe("dual_sequence");
        expect(resp.data.reset).toBeFalsy();
        done();
      },
      ws.send(JSON.stringify({
          params: { prune_threshold: 0 },
          reset: false,
          event_id: 0,
          action: 'node_tree'
      }));
    });
  });

  //Check that we receive a simple node tree
  test("Receive simple node tree -- reset 'true'", (done) => {
    wsFactory().then((ws) => {
      expect(ws).toBeTruthy();
      if(!ws)
        return;

      ws.onmessage = (event) => {
        let resp = JSON.parse(event.data);
        expect(resp.type).toBe("nodetree");
        expect(resp.data.name).toBe("_root");
        expect(resp.data.children.length).toBeGreaterThan(0);
        expect(resp.data.children[0].name).not.toBe("dual_sequence");
        expect(resp.data.reset).toBeTruthy();
        done();
      },
      ws.send(JSON.stringify({
          params: { prune_threshold: 0 },
          reset: true,
          event_id: 0,
          action: 'node_tree'
      }));
    });
  });

  test("Test empty root -- no alignment", (done) => {
    wsFactory().then((ws) => {
      expect(ws).toBeTruthy();
      if(!ws)
        return;

      ws.onmessage = (event) => {
        let resp = JSON.parse(event.data);
        expect(resp.type).toBe("nodetree");
        expect(resp.data.name).toBe("_root");
        expect(resp.data.children.length).toBe(0);
        done();
      },
      ws.send(JSON.stringify({
          params: { prune_threshold: 1e9 },
          reset: true,
          event_id: 0,
          action: 'node_tree'
      }));
    });
  });

  //Check the dual sequence thing
  test("Receive dual_sequence node tree", (done) => {
    wsFactory().then((ws) => {
      expect(ws).toBeTruthy();
      if(!ws)
        return; //Nothing more to do...

      ws.onmessage = (event) => {
        let resp = JSON.parse(event.data);
        expect(resp.type).toBe("nodetree");
        expect(resp.data.name).toBe("_root");
        expect(resp.data.children.length).toBeGreaterThan(0);
        expect(resp.data.children[0].name).toBe("dual_sequence");
        expect(resp.data.children[0].children[0].name).toBe("after");
        expect(resp.data.children[0].children[1].name).toBe("before");
        done();
      },
      ws.send(JSON.stringify({
          params: { align_sequence: [1 << 3], prune_threshold: 0 }, //1 << 3 == surgery
          reset: true,
          event_id: 0,
          action: 'node_tree'
      }));
    });
  });

  //Check the dual sequence thing
  test("Receive empty dual_sequence node tree", (done) => {
    wsFactory().then((ws) => {
      expect(ws).toBeTruthy();
      if(!ws)
        return; //Nothing more to do...

      ws.onmessage = (event) => {
        let resp = JSON.parse(event.data);
        expect(resp.type).toBe("nodetree");
        expect(resp.data.name).toBe("_root");
        expect(resp.data.children[0].name).toBe("dual_sequence");
        expect(resp.data.children[0].children[0].name).toBe("after");
        expect(resp.data.children[0].children[1].name).toBe("before");
        for(let i = 0; i < 2; i++)
          expect(resp.data.children[0].children[i].children.length).toBe(0);
        expect(resp.data.count).toBe(0);
        expect(resp.data.align_sequence).toStrictEqual([1<<3, 1<<3]);
        done();
      },
      ws.send(JSON.stringify({
          params: { align_sequence: [1 << 3, 1 << 3]}, //1 << 3 == surgery
          reset: true,
          event_id: 0,
          action: 'node_tree'
      }));
    });
  });
})
