import { mount, createLocalVue } from '@vue/test-utils';

import Vuetify from 'vuetify';
import Vuex from 'vuex';
import Vue from 'vue';

import * as d3 from "d3";

import * as treeUtils from "@/treeUtils";
import { getters } from "@/plugins/vuex";
import SequenceView from '@/components/SequenceView.vue';
import { ViewType, TreeToDisplay } from '@/Config';
import { parseTransform, generateSequenceViewTree } from '../utils';
import { searchNodeByID } from '@/treeUtils';

Vue.use(Vuetify);
Vue.use(Vuex)

const localVue = createLocalVue();
const d3SelectionProto = Object.assign({}, d3.selection.prototype);

describe('SequenceView.vue', () => {
  let state
  let actions;
  let mutations;

  let store;
  let vuetify;

  beforeEach(() => {
    //Disable animation of D3. Indeed, issue with transitions (due to JSDOM) otherwise for unit testing ONLY, see https://stackoverflow.com/questions/14443724/disabling-all-d3-animations-for-testing
    d3.selection.prototype.duration   = function(){ return this }
    d3.selection.prototype.transition = function(){ return this }
    d3.selection.prototype.on = function(event, callback) {
      if (event === 'start' || event === 'end') {
        this.each(callback);
        return this;
      } else {
        return d3SelectionProto.on.apply(this, arguments);
      }
    };

    state = {
      view_type                        : ViewType.SEQUENCE_VIEW_TYPE,
      current_tree                     : null,
      display                          : {
                                            main : {
                                              width    : 1920,
                                              height   : 1080,
                                              padding  : {
                                                left   : 0,
                                                top    : 0,
                                                right  : 0,
                                                bottom : 0
                                              },
                                              innerWidth  : 1920,
                                              innerHeight : 1080,
                                            }
                                         },
      display_flag                     : 0,
      main_view_zoomed_node_id         : 0,
      align_sequence                   : ["alphabloc"],
      align_sequence_by                : 0,
      sequence_view_zoomed_nodes_ids   : [0, 0],
      sequence_view_curtree_to_display : TreeToDisplay.BEFORE,
    };
    actions = {
      get_distribution                   : jest.fn(),
      set_sequence_view_zoomed_nodes_ids : jest.fn(),
    };
    mutations = {
      set_current_sequence                 : jest.fn(),
      set_tab                              : jest.fn(),
      set_sequence_view_curtree_to_display : jest.fn(),
    };
    store = new Vuex.Store({
      state,
      actions,
      mutations,
      getters,
    });
    vuetify = new Vuetify();

    jest.useRealTimers();
  });

  function wrapperFactory() {
    const wrapper = mount(SequenceView, {store, localVue, vuetify});
    return wrapper;
  }

  test("Visibility", async () => {
    state.current_tree = generateSequenceViewTree();
    const wrapper = wrapperFactory();
    expect(d3.select(wrapper.vm.$refs['view_before']).size()).toBe(1);
    expect(d3.select(wrapper.vm.$refs['view_after']).size()).toBe(1);

    //Check what is related to the "before" view
    expect(wrapper.findComponent({ref: "view_before"}).isVisible()).toBeTruthy();
    expect(wrapper.findComponent({ref: "view_after"}).isVisible()).toBeFalsy();
    expect(wrapper.find("#showBeforeBtn").isVisible()).toBeFalsy();
    expect(wrapper.find("#showAfterBtn").isVisible()).toBeTruthy();
    expect(wrapper.find("#alignSeq_before").isVisible()).toBeTruthy();
    expect(wrapper.find("#alignSeq_after").isVisible()).toBeFalsy();

    //Check what is related to the "after" view
    state.sequence_view_curtree_to_display = TreeToDisplay.AFTER;
    await Vue.nextTick();

    expect(wrapper.findComponent({ref: "view_after"}).isVisible()).toBeTruthy();
    expect(wrapper.findComponent({ref: "view_before"}).isVisible()).toBeFalsy();
    expect(wrapper.find("#showAfterBtn").isVisible()).toBeFalsy();
    expect(wrapper.find("#showBeforeBtn").isVisible()).toBeTruthy();
    expect(wrapper.find("#alignSeq_after").isVisible()).toBeTruthy();
    expect(wrapper.find("#alignSeq_before").isVisible()).toBeFalsy();
  });

  test("Hierarchy X -- View Before", async() => {
    state.current_tree = generateSequenceViewTree();
    const wrapper = wrapperFactory();

    let svg = d3.select(wrapper.vm.$refs['view_before']);
    let nbParents = 0;
    svg.select(".content").selectAll("rect").each(function(data, _ind) {
      //Check that children nodes are always placed LEFT to parents nodes
      if(data.parent != null) {
        nbParents++;
        let parentNode = svg.select(".content").selectAll("rect").filter((d) => (d.data.children.find((c) => c.id == data.data.id) != null));
        let parentX     = parseFloat(parseTransform(parentNode.attr('transform')).translate[0]); 
        let parentWidth = parseFloat(parentNode.attr('width')); 
        let childX      = parseFloat(parseTransform(d3.select(this).attr('transform')).translate[0]); 

        expect(parentX + parentWidth).toBeGreaterThanOrEqual(childX);
      }
    });

    expect(nbParents).toBe(4); //Total nodes minus root
                               //Remember that the "root", in this case, is of type "before" and not "root" (we skip align_sequence and root)
  });

  test("Hierarchy X -- View After", async() => {
    state.current_tree = generateSequenceViewTree();
    state.sequence_view_curtree_to_display = TreeToDisplay.AFTER;
    const wrapper = wrapperFactory();

    let svg = d3.select(wrapper.vm.$refs['view_after']);
    let nbParents = 0;
    svg.select(".content").selectAll("rect").each(function(data, _ind) {
      //Check that children nodes are always placed LEFT to parents nodes
      if(data.parent != null) {
        nbParents++;
        let parentNode = svg.select(".content").selectAll("rect").filter((d) => (d.data.children.find((c) => c.id == data.data.id) != null));
        let parentX     = parseFloat(parseTransform(parentNode.attr('transform')).translate[0]); 
        let parentWidth = parseFloat(parentNode.attr('width')); 
        let childX      = parseFloat(parseTransform(d3.select(this).attr('transform')).translate[0]); 

        expect(parentX + parentWidth).toBeLessThanOrEqual(childX);
      }
    });

    expect(nbParents).toBe(3); //Total nodes minus root
                               //Remember that the "root", in this case, is of type "before" and not "root" (we skip dual_sequence and root)
  });

  test("Hierarchy Y", async() => {
    state.current_tree = generateSequenceViewTree();
    const wrapper = wrapperFactory();

    let svg = d3.select(wrapper.vm.$refs['view_before']);
    let nodes = svg.select(".content").selectAll("rect");

    function checkYRec(curNode) {
      for(let i = 0; i < curNode.children.length-1; i++) {
        let nD3      = nodes.filter((d) => d.data.id == curNode.children[i].id);
        let nHeight  = parseFloat(nD3.attr('height'));
        let nY       = parseFloat(parseTransform(nD3.attr('transform')).translate[1]);
        let nPlus1_Y = parseFloat(parseTransform(nodes.filter((d) => d.data.id == curNode.children[i+1].id).attr('transform')).translate[1]);
        expect(nY + nHeight).toBeLessThanOrEqual(nPlus1_Y); 
        checkYRec(curNode.children[i]);
      }
      if(curNode.children.length > 0)
        checkYRec(curNode.children[curNode.children.length-1]);
    }

    checkYRec(state.current_tree.children[0].children[1]); //Check before
   
    state.sequence_view_curtree_to_display = TreeToDisplay.AFTER;
    await Vue.nextTick();
    svg = d3.select(wrapper.vm.$refs['view_after']);
    nodes = svg.select(".content").selectAll("rect");
    checkYRec(state.current_tree.children[0].children[0]); //Check after
  });

  test("Click -- Before", async () => {
    state.current_tree = generateSequenceViewTree();
    const wrapper = wrapperFactory();

    let svg   = d3.select(wrapper.vm.$refs['view_before']);
    let nodes = svg.select(".content").selectAll("rect");

    function checkClickNode_rec(node, nthCall, parentPrefix) {
      let d3Node = nodes.filter((d) => d.data.id == node.id);
      d3Node.node().dispatchEvent(new Event('click'));
      nthCall++;
      expect(svg.select(".content").selectAll(".selectedNode").size()).toBe(1);
      expect(svg.select(".content").selectAll(".selectedNode").node()).toBe(d3Node.node());
      expect(mutations.set_current_sequence).toHaveBeenCalledTimes(nthCall);
      expect(mutations.set_tab).toHaveBeenCalledTimes(nthCall);
      expect(mutations.set_current_sequence).toHaveBeenLastCalledWith(expect.anything(), [node.name, ...parentPrefix]);
      expect(actions.get_distribution).toHaveBeenCalledTimes(nthCall);
      expect(actions.get_distribution.mock.calls[nthCall-1][1].action).toBe("distribution");
      expect(actions.get_distribution.mock.calls[nthCall-1][1].node_id).toBe(node.id);
      for(let i = 0; i < node.children.length; i++)
        nthCall = checkClickNode_rec(node.children[i], nthCall, [node.name, ...parentPrefix]);

      return nthCall;
    }

    let root = state.current_tree.children[0].children[1];
    let nthCall = 0;
    for(let i = 0; i < root.children.length; i++)
      nthCall = checkClickNode_rec(root.children[i], nthCall, []);
    expect(nthCall).toBe(4); //all minus root

    //Zoom on something
    let newD3Root = nodes.filter((d) => d.data.id == 5);
    newD3Root.node().dispatchEvent(new Event('dblclick'));
    expect(actions.set_sequence_view_zoomed_nodes_ids).toHaveBeenCalledTimes(1);
    expect(actions.set_sequence_view_zoomed_nodes_ids).toHaveBeenLastCalledWith(expect.anything(), {ids: [5, 0]});
    state.sequence_view_zoomed_nodes_ids = [5, 0];
    await Vue.nextTick();

    //Check zoom_in prefix for a leaf AFTER root (size == 1)
    expect(wrapper.find("#zoomInfo_before").exists()).toBeTruthy();
    expect(wrapper.findAll("#zoomInfo_before .zoomInSpan").length).toBe(1);
    expect(wrapper.findAll("#zoomInfo_before .zoomInSpan").at(0).text("")).toBe("surgery");
    
    //...check that all are visible
    nodes = d3.select(wrapper.vm.$refs['view_before']).select(".content").selectAll("rect");
    expect(nodes.size()).toBe(2);
    
    //...and check again the prefix
    let newRoot = root.children[1];
    nthCall = checkClickNode_rec(newRoot, nthCall, []);
    expect(nthCall).toBe(2+4); //Two more calls
    
    //Zoom on a leaf
    newD3Root = nodes.filter((d) => d.data.id == 6);
    newD3Root.node().dispatchEvent(new Event('dblclick'));
    expect(actions.set_sequence_view_zoomed_nodes_ids).toHaveBeenCalledTimes(2);
    expect(actions.set_sequence_view_zoomed_nodes_ids).toHaveBeenLastCalledWith(expect.anything(), {ids: [6, 0]});
    state.sequence_view_zoomed_nodes_ids = [6, 0];
    await Vue.nextTick();

    //Check zoom_in prefix for a leaf AFTER root (size == 0)
    expect(wrapper.find("#zoomInfo_before").exists()).toBeTruthy();
    expect(wrapper.findAll("#zoomInfo_before .zoomInSpan").length).toBe(2);
    expect(wrapper.findAll("#zoomInfo_before .zoomInSpan").at(1).text()).toBe("surgery");
    expect(wrapper.findAll("#zoomInfo_before .zoomInSpan").at(0).text()).toBe("fivealpha phyto");
    
    //...check that all are visible
    nodes = d3.select(wrapper.vm.$refs['view_before']).select(".content").selectAll("rect");
    expect(nodes.size()).toBe(1); //Show only the leaf

    //...and check again the prefix
    //However, this leaf was "clicked" before. We need to "unclick" it
    nodes.node().dispatchEvent(new Event('click'));
    nthCall++;
    expect(svg.select(".content").selectAll(".selectedNode").size()).toBe(0);
    newRoot = root.children[1].children[0];
    nthCall = checkClickNode_rec(newRoot, nthCall, [root.children[1].name]);
    expect(nthCall).toBe(1+7); //One more calls
    
    //Unzoom
    svg.node().dispatchEvent(new Event('dblclick'));
    expect(actions.set_sequence_view_zoomed_nodes_ids).toHaveBeenCalledTimes(3);
    expect(actions.set_sequence_view_zoomed_nodes_ids).toHaveBeenLastCalledWith(expect.anything(), {ids: [-1, 0]});
    state.sequence_view_zoomed_nodes_ids = [-1, 0];
    await Vue.nextTick();

    //Check zoom_in prefix for unzoomed view
    expect(wrapper.find("#zoomInfo_before").exists()).toBeTruthy();
    expect(wrapper.findAll("#zoomInfo_before .zoomInSpan").length).toBe(0);

    //Unzooming again does nothing
    svg.node().dispatchEvent(new Event('dblclick'));
    expect(actions.set_sequence_view_zoomed_nodes_ids).toHaveBeenCalledTimes(3);
  });

  test("Click -- After", async () => {
    state.current_tree = generateSequenceViewTree();
    state.sequence_view_curtree_to_display = TreeToDisplay.AFTER;
    const wrapper = wrapperFactory();

    let svg = d3.select(wrapper.vm.$refs['view_after']);
    let nodes = svg.select(".content").selectAll("rect");

    function checkClickNode_rec(node, nthCall, parentPrefix) {
      let d3Node = nodes.filter((d) => d.data.id == node.id);
      d3Node.node().dispatchEvent(new Event('click'));
      nthCall++;
      expect(svg.select(".content").selectAll(".selectedNode").size()).toBe(1);
      expect(svg.select(".content").selectAll(".selectedNode").node()).toBe(d3Node.node());
      expect(mutations.set_current_sequence).toHaveBeenCalledTimes(nthCall);
      expect(mutations.set_current_sequence).toHaveBeenLastCalledWith(expect.anything(), [...parentPrefix, node.name]);
      expect(actions.get_distribution).toHaveBeenCalledTimes(nthCall);
      expect(actions.get_distribution.mock.calls[nthCall-1][1].action).toBe("distribution");
      expect(actions.get_distribution.mock.calls[nthCall-1][1].node_id).toBe(node.id);
      for(let i = 0; i < node.children.length; i++)
        nthCall = checkClickNode_rec(node.children[i], nthCall, [...parentPrefix, node.name]);

      return nthCall;
    }

    let root = state.current_tree.children[0].children[0];
    let nthCall = 0;
    for(let i = 0; i < root.children.length; i++)
      nthCall = checkClickNode_rec(root.children[i], nthCall, []);
    expect(nthCall).toBe(3); //all minus root

    //Zoom on something
    let newD3Root = nodes.filter((d) => d.data.id == 8);
    newD3Root.node().dispatchEvent(new Event('dblclick'));
    expect(actions.set_sequence_view_zoomed_nodes_ids).toHaveBeenCalledTimes(1);
    expect(actions.set_sequence_view_zoomed_nodes_ids).toHaveBeenLastCalledWith(expect.anything(), {ids: [0, 8]});
    state.sequence_view_zoomed_nodes_ids = [0, 8];
    await Vue.nextTick();

    //Check zoom_in prefix for a node AFTER root (size == 1)
    expect(wrapper.find("#zoomInfo_after").exists()).toBeTruthy();
    expect(wrapper.findAll("#zoomInfo_after .zoomInSpan").length).toBe(1);
    expect(wrapper.findAll("#zoomInfo_after .zoomInSpan").at(0).text("")).toBe("alphabloc");
    
    //...check that all are visible
    nodes = d3.select(wrapper.vm.$refs['view_after']).select(".content").selectAll("rect");
    expect(nodes.size()).toBe(2);
    
    //...and check again the prefix
    let newRoot = root.children[0];
    nthCall = checkClickNode_rec(newRoot, nthCall, []);
    expect(nthCall).toBe(2+3); //Two more calls
    
    //Zoom on a leaf
    newD3Root = nodes.filter((d) => d.data.id == 9);
    newD3Root.node().dispatchEvent(new Event('dblclick'));
    expect(actions.set_sequence_view_zoomed_nodes_ids).toHaveBeenCalledTimes(2);
    expect(actions.set_sequence_view_zoomed_nodes_ids).toHaveBeenLastCalledWith(expect.anything(), {ids: [0, 9]});
    state.sequence_view_zoomed_nodes_ids = [0, 9];
    await Vue.nextTick();

    //Check zoom_in prefix for a leaf AFTER root (size == 0)
    expect(wrapper.find("#zoomInfo_after").exists()).toBeTruthy();
    expect(wrapper.findAll("#zoomInfo_after .zoomInSpan").length).toBe(2);
    expect(wrapper.findAll("#zoomInfo_after .zoomInSpan").at(0).text()).toBe("alphabloc");
    expect(wrapper.findAll("#zoomInfo_after .zoomInSpan").at(1).text()).toBe("phyto");
    
    //...check that all are visible
    nodes = d3.select(wrapper.vm.$refs['view_after']).select(".content").selectAll("rect");
    expect(nodes.size()).toBe(1); //Show only the leaf

    //...and check again the prefix
    //However, this leaf was "clicked" before. We need to "unclick" it
    nodes.node().dispatchEvent(new Event('click'));
    nthCall++;
    expect(svg.select(".content").selectAll(".selectedNode").size()).toBe(0);
    newRoot = root.children[0].children[0];
    nthCall = checkClickNode_rec(newRoot, nthCall, [root.children[0].name]);
    expect(nthCall).toBe(1+6); //One more calls
    
    //Unzoom
    svg.node().dispatchEvent(new Event('dblclick'));
    expect(actions.set_sequence_view_zoomed_nodes_ids).toHaveBeenCalledTimes(3);
    expect(actions.set_sequence_view_zoomed_nodes_ids).toHaveBeenLastCalledWith(expect.anything(), {ids: [0, -1]});
    state.sequence_view_zoomed_nodes_ids = [0, -1];
    await Vue.nextTick();

    //Check zoom_in prefix for unzoomed view
    expect(wrapper.find("#zoomInfo_after").exists()).toBeTruthy();
    expect(wrapper.findAll("#zoomInfo_after .zoomInSpan").length).toBe(0);

    //Unzooming again does nothing
    svg.node().dispatchEvent(new Event('dblclick'));
    expect(actions.set_sequence_view_zoomed_nodes_ids).toHaveBeenCalledTimes(3);
  });

  test("Zoomed on updated trees", async () => {
    state.current_tree = generateSequenceViewTree();
    state.sequence_view_zoomed_nodes_ids = [3, 8]; //Respectively ids 1 (before on its own ref) and 1 (after on its own ref)
    const wrapper = wrapperFactory();

    //Generate a new tree similar to the ancient one but with modified count
    let newTree = generateSequenceViewTree();
    let beforeSelectedNode = searchNodeByID(newTree, state.sequence_view_zoomed_nodes_ids[0]);
    let afterSelectedNode  = searchNodeByID(newTree, state.sequence_view_zoomed_nodes_ids[1]);

    beforeSelectedNode.count += 5;
    afterSelectedNode.count  += 5;

    //Apply the changes
    state.current_tree = newTree;
    await Vue.nextTick();

    //Check that the view is still zoomed
    expect(wrapper.vm.curtreeBefore).toBe(beforeSelectedNode);
    expect(wrapper.vm.curtreeAfter).toBe(afterSelectedNode);
  });

  test("Progressive tooltip", async () => {
    state.current_tree = generateSequenceViewTree();
    const wrapper = wrapperFactory();

    let svg     = d3.select(wrapper.vm.$refs['view_before']);
    let nodes   = svg.select(".content").selectAll("rect");
    let tooltip = wrapper.vm.$refs['tooltip'];

    let check_rec = async function(node) {
      let d3Node = nodes.filter((d) => d.data.id == node.id);

      //Test mouseover
      d3Node.node().dispatchEvent(new Event('mouseover'));
      let textContent = tooltip.textContent;
      expect(textContent).toContain(node.name);
      expect(parseFloat(tooltip.style.opacity)).toBeGreaterThan(0.1);
      expect(tooltip.style.display).not.toBe("none");

      //Set the node data
      node.count *= 2;
      state.current_tree = {...state.current_tree};
      node = treeUtils.searchNodeByID(state.current_tree, node.id);
      await Vue.nextTick();
      expect(textContent).not.toBe(tooltip.textContent);
      expect(tooltip.textContent).toContain(node.name);

      d3Node.node().dispatchEvent(new Event('mouseout'));
      expect(parseFloat(tooltip.style.opacity) == 0.0 || tooltip.style.display == "none").toBeTruthy();

      for(let i = 0; i < node.children.length; i++)
        await check_rec(node.children[i]);
    };

    //Test the tree "BEFORE" and "AFTER"!
    let root = state.current_tree.children[0].children[1];
    await Vue.nextTick();
    for(let i = 0; i < root.children.length; i++)
      await check_rec(root.children[i]);

    state.sequence_view_curtree_to_display = TreeToDisplay.AFTER;
    await Vue.nextTick();
    svg   = d3.select(wrapper.vm.$refs['view_after']);
    nodes = svg.select(".content").selectAll("rect");
    root  = state.current_tree.children[0].children[0];
    for(let i = 0; i < root.children.length; i++)
      await check_rec(root.children[i]);

  });
});
