import * as Tooltip from '@/components/Tooltip';
import * as d3 from 'd3';

const d3SelectionProto = Object.assign({}, d3.selection.prototype);

describe("Tooltip", () => {
  let tooltip;

  beforeEach(() => {
    tooltip = document.createElement("div");
    tooltip = d3.select(tooltip);

    d3.selection.prototype.duration   = function(){ return this }
    d3.selection.prototype.transition = function(){ return this }
    d3.selection.prototype.on = function(event, callback) {
      if (event === 'start' || event === 'end') {
        this.each(callback);
        return this;
      } else {
        return d3SelectionProto.on.apply(this, arguments);
      }
    };
  });

  test("reinit", () => {
    Tooltip.reinit(tooltip);
    expect(parseFloat(tooltip.style("opacity")) == 0.0 || tooltip.style("display") == "none").toBeTruthy();
  });

  test("mouseover -- mouseout", () => {
    Tooltip.reinit(tooltip);

    let mouseEvent = new MouseEvent("mouseenter", {clientX: 0, clientY: 0});

    Tooltip.mouseover(tooltip, mouseEvent);
    expect(parseFloat(tooltip.style("opacity"))).toBeGreaterThan(0.1);
    expect(tooltip.style("display")).not.toBe("none");

    Tooltip.mouseout(tooltip, mouseEvent);
    expect(parseFloat(tooltip.style("opacity")) == 0.0 || tooltip.style("display") == "none").toBeTruthy();
  });
});
