# Front end code structure

The front end mainly relies on Vue.js (v2.6) and d3 (v6). The source code is transpiled using babel.

## Vuex to store the data

The application uses a main data structure (src/plugins/vuex.js) that stores the whole app status. This data structure is used by all Vue's components and handles the communication with the backend.

This data structure also gives default width and height for all the subcomponents. Some of the metrics are hardcoded in pixels, which should be improved later.

## Vuetify to have a nice UI

We rely on Vuetify for all the "common" GUI components (buttons, sliders, tabs, etc.). The default theme configuration is done in src/plugins/vuetify.js

## Vue components

All the vue components are in src/components, except the main component "src/App.vue". All uses the vuex store that src/plugins/vuex.js creates.

When the application starts, Vue.js automatically injects its components into the initial DOM described in public/index.html. This injection performed in src/main.js which is the entrypoint of the application.

### The main visualization

The two main visualizations parcoursvis creates are in src/components/MainView.vue and src/components/SequenceView.vue. They both rely on "src/components/DrawTree.js" to render the aggregated trees.

# Test

We describe in this section all the automatic tests we perform as part of our continuous integration. For the moment, we only support unit testing

## Unit testing

The unit tests relies on jest. All the files related to the unit testing are in src/tests/unit. The purpose of those tests is to test the DOM elements, and also some scripts that performs computations instead of rendering (e.g., how the History behave in src/History.js)
